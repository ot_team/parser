<?php
include_once('simple_html_dom.php');
echo "Hello, DCCXI";
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('max_execution_time', 9000);
date_default_timezone_set('Europe/Kiev');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("OT")
    ->setLastModifiedBy("PBlazhuk")
    ->setTitle("Companies list")
    ->setSubject("Companies")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

// Set default font
echo date('H:i:s') , " Set default font" , EOL;
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
    ->setSize(10);

// Add some data, resembling some different data types
echo date('H:i:s') , " Add some data" , EOL;

$key_cell = 'A4';
$counter = 5;



$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Назва')
    ->setCellValue('B1', 'Категорія')
    ->setCellValue('C1', 'Адреса')
    ->setCellValue('D1', 'E-mail')
    ->setCellValue('E1', 'Телефон')
    ->setCellValue('F1', 'Сайт')
    ->setCellValue('G1', 'Соц. мережі');




$citiesInfo = array(
    'cities' => array(
        /*  0 => array(
              'title' => 'Вінниця', 'url' => 'https://20.ua/vn', 'categories' => array(
              /* 0 => array(
                      'cat_name' => 'Здоров’я',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1414&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/zdorove.html'
                  ),
                   1 => array(
                      'cat_name' => 'Авто, мото',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=38&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/avto-moto.html'
                  ),
                  2 => array(
                      'cat_name' => 'Будівництво та ремонт',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=35&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/stroitelstvo-i-remont.html'
                  ),
                  3 => array(
                      'cat_name' => 'Транспортні послуги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1410&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/transportnyie-uslugi.html'
                  ),
                  4 => array(
                      'cat_name' => 'Ресторани, кафе, бари',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1489&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/restoranyi-kafe-baryi.html'
                  ),
                  5 => array(
                      'cat_name' => 'Дозвілля, розваги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1516&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/razvlecheniya.html'
                  ),
                  6 => array(
                      'cat_name' => 'Бізнес послуги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=154&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/biznes-uslugi.html'
                  ),
                  7 => array(
                      'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1429&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/kompyuternaya-ofisnaya-i-byitovaya-tehnika.html'
                  ),
                  8 => array(
                      'cat_name' => 'Магазини, супермаркети',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1000&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/magazinyi-supermarketyi.html'
                  ),
                 9 => array(
                      'cat_name' => 'Організація заходів',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=118&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/organizatsiya-meropriyatiy.html'
                  ),
                   10 => array(
                      'cat_name' => 'Навчання, курси',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1470&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/obuchenie.html'
                  ),
                  11 => array(
                      'cat_name' => 'Торгове та промислове обладнання',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1009&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/torgovoe-i-promyishlennoe-oborudovanie.html'
                  ),
                 12 => array(
                      'cat_name' => 'Дім і побут',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1421&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/dom-i-byit.html'
                  ),
                   13 => array(
                      'cat_name' => 'Спорт, краса',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1416&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/sport-krasota.html'
                  ),
                  14 => array(
                      'cat_name' => 'Туризм, подорожі',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1493&query%5Btag%5D=&query%5Bcity%5D=4',
                      'url' => 'https://20.ua/vn/turizm-puteshestvie.html'
                  ),*/
        /* 15 => array(
             'cat_name' => 'Держустанови, комунальні служби',
             'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1449&query%5Btag%5D=&query%5Bcity%5D=4',
             'url' => 'https://20.ua/vn/gosucherezhdeniya-kommunalnyie-sluzhbyi.html'
         ),
     ),
 ),*/
        /*     1 => array(
                'title' => 'Житомир', 'url' => 'https://20.ua/zh', 'categories' => array(
                 0 => array(
                         'cat_name' => 'Здоров’я',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=847&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/krasotazdorove.html'
                     ),
                     1 => array(
                         'cat_name' => 'Авто, мото',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=814&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/avto-moto.html'
                     ),
                     2 => array(
                         'cat_name' => 'Будівництво та ремонт',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=802&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/stroitelstvo-i-remont.html'
                     ),
                    3 => array(
                         'cat_name' => 'Транспортні послуги',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1839&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/transportni-poslugi.html'
                     ),
                    4 => array(
                         'cat_name' => 'Ресторани, кафе, бари',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1836&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/restorani-kafe-bari.html'
                     ),
                       5 => array(
                         'cat_name' => 'Дозвілля, розваги',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=824&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/dosug-razvlecheniya.html'
                     ),
                     6 => array(
                         'cat_name' => 'Бізнес послуги',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=885&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/biznes-uslugi.html'
                     ),
                     7 => array(
                         'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1834&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/kompyuterna-ofisna-ta-pobutova-tehnika.html'
                     ),
                    8 => array(
                         'cat_name' => 'Магазини, супермаркети',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1023&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/magazinyi-supermarketyi.html'
                     ),
                      9 => array(
                         'cat_name' => 'Організація заходів',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=831&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/organizatsiya-meropriyatiy.html'
                     ),
                     10 => array(
                         'cat_name' => 'Навчання, курси',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=773&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/obuchenie-kursyi.html'
                     ),
                     11 => array(
                         'cat_name' => 'Торгове та промислове обладнання',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1029&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/torgovoe-i-promyishlennoe-oborudovanie.html'
                     ),
                    12 => array(
                         'cat_name' => 'Дім і побут',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=792&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/uslugi-po-domu-i-byitu.html'
                     ),
                     13 => array(
                         'cat_name' => 'Спорт, краса',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=763&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/kruzhki-sportivnyie-sektsii.html'
                     ),
                      14 => array(
                         'cat_name' => 'Туризм, подорожі',
                         'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=859&query%5Btag%5D=&query%5Bcity%5D=5',
                         'url' => 'https://20.ua/zh/turizm.html'
                     ),
                     15 => array(
                          'cat_name' => 'Держустанови, комунальні служби',
                          'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=867&query%5Btag%5D=&query%5Bcity%5D=5',
                          'url' => 'https://20.ua/zh/gosuchrezhdeniya.html'
                      ),
                ),
            ),*/
        /*  2 => array(
              'title' => 'Тернопіль', 'url' => 'https://20.ua/tr', 'categories' => array(
               /* 0 => array(
                      'cat_name' => 'Здоров’я',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=504&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/krasotazdorove.html'
                  ),
                 1 => array(
                      'cat_name' => 'Авто, мото',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=462&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/avto-moto.html'
                  ),
                   2 => array(
                      'cat_name' => 'Транспортні послуги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1838&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/transportni-poslugi.html'
                  ),
                 3 => array(
                      'cat_name' => 'Бізнес послуги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=526&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/biznes-uslugi.html'
                  ),
                   4 => array(
                      'cat_name' => 'Ресторани, кафе, бари',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1835&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/restorani-kafe-bari.html'
                  ),
                  5 => array(
                      'cat_name' => 'Дозвілля, розваги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=490&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/dosug-razvlecheniya.html'
                  ),
                 6 => array(
                      'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1833&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/kompyuterna-ofisna-ta-pobutova-tehnika.html'
                  ),
                   7 => array(
                      'cat_name' => 'Навчання, курси',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=459&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/obuchenie.html'
                  ),
                  8 => array(
                      'cat_name' => 'Магазини, супермаркети',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1042&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/magazinyi-supermarketyi.html'
                  ),
                9 => array(
                      'cat_name' => 'Організація заходів',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=494&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/organizatsiya-meropriyatiy.html'
                  ),
                    10 => array(
                      'cat_name' => 'Торгове та промислове обладнання',
                      'ajax' => '',
                      'url' => 'https://20.ua/tr/torgove-ta-promislove-obladnannya.html'
                  ),
                 11 => array(
                      'cat_name' => 'Туризм, подорожі',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=515&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/turizm.html'
                  ),
                   12 => array(
                      'cat_name' => 'Дім і побут',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=460&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/uslugi-po-domu-byitu.html'
                  ),
                 13 => array(
                      'cat_name' => 'Спорт, краса',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=458&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/kruzhki-sektsii.html'
                  ),
                   14 => array(
                      'cat_name' => 'Держустанови, комунальні служби',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=522&query%5Btag%5D=&query%5Bcity%5D=6',
                      'url' => 'https://20.ua/tr/gosuchrezhdeniya.html'
                  ),*/
        /*15 => array(
            'cat_name' => 'Будівництво та ремонт',
            'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=461&query%5Btag%5D=&query%5Bcity%5D=6',
            'url' => 'https://20.ua/tr/stroitelstvo-i-remont.html'
        ),
    ),
),*/
        /* 3 => array(
             'title' => 'Хмельницький', 'url' => 'https://20.ua/khm', 'categories' => array(
               /*0 => array(
                     'cat_name' => 'Здоров’я',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1679&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/zdorove.html'
                 ),
                 1 => array(
                     'cat_name' => 'Авто, мото',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=661&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/avto-moto.html'
                 ),
                 2 => array(
                     'cat_name' => 'Будівництво та ремонт',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=654&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/stroitelstvo-i-remont.html'
                 ),
                 3 => array(
                     'cat_name' => 'Транспортні послуги',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1657&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/transportnyie-uslugi.html'
                 ),
                 4 => array(
                     'cat_name' => 'Ресторани, кафе, бари',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1669&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/zavedeniya-pitaniya.html'
                 ),
                 5 => array(
                     'cat_name' => 'Дозвілля, розваги',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1663&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/razvlecheniya.html'
                 ),
                 6 => array(
                     'cat_name' => 'Бізнес послуги',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=722&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/biznes-uslugi.html'
                 ),
                 7 => array(
                     'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1640&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/kompyuternaya-ofisnaya-i-byitovaya-tehnika.html'
                 ),
                 8 => array(
                     'cat_name' => 'Магазини, супермаркети',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1053&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/magazinyi-supermarketyi.html'
                 ),
                 9 => array(
                     'cat_name' => 'Організація заходів',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=679&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/organizatsiya-meropriyatiy.html'
                 ),
                 10 => array(
                     'cat_name' => 'Навчання, курси',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1626&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/obuchenie.html'
                 ),
                 11 => array(
                     'cat_name' => 'Торгове та промислове обладнання',
                     'ajax' => '',
                     'url' => 'https://20.ua/khm/torgovoe-i-promyishlennoe-oborudovanie.html'
                 ),
                 12 => array(
                     'cat_name' => 'Дім і побут',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1634&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/dom-i-byit.html'
                 ),
                13 => array(
                     'cat_name' => 'Спорт, краса',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1622&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/sport-krasota.html'
                 ),
                  14 => array(
                     'cat_name' => 'Туризм, подорожі',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1666&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/turizm-puteshestviya.html'
                 ),
                 15 => array(
                     'cat_name' => 'Держустанови, комунальні служби',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1672&query%5Btag%5D=&query%5Bcity%5D=7',
                     'url' => 'https://20.ua/khm/gosuchrezhdeniya-kommunalnyie-sluzhbyi.html'
                 ),
             ),
         ),*/
        /*  4 => array(
            'title' => 'Рівне', 'url' => 'http://catalog.ogo.ua/', 'categories' => array(
             0 => array(
                    'cat_name' => 'Здоров’я',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1553&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/zdorovya.html'
                ),
                1 => array(
                    'cat_name' => 'Авто, мото',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1523&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/avto-moto.html'
                ),
                2 => array(
                    'cat_name' => 'Будівництво та ремонт',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1531&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/stroitelstvo-i-remont.html'
                ),
               3 => array(
                    'cat_name' => 'Транспортні послуги',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1549&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/transportnyie-uslugi.html'
                ),
                 4 => array(
                    'cat_name' => 'Ресторани, кафе, бари',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1602&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/pitaniya.html'
                ),
                 5 => array(
                    'cat_name' => 'Дозвілля, розваги',
                    'ajax' => '',
                    'url' => 'http://catalog.ogo.ua/rv/razvlecheniya.html'
                ),
              6 => array(
                    'cat_name' => 'Бізнес послуги',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1536&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/biznes-uslugi.html'
                ),
                7 => array(
                    'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1555&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/kompyuternaya-ofisnaya-i-byitovaya-tehnika.html'
                ),
                  8 => array(
                    'cat_name' => 'Магазини, супермаркети',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1576&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/magazinyi-supermarketyi.html'
                ),
                9 => array(
                    'cat_name' => 'Організація заходів',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1547&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/organizatsiya-meropriyatiy.html'
                ),
                10 => array(
                    'cat_name' => 'Навчання, курси',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1529&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/obuchenie.html'
                ),
                11 => array(
                    'cat_name' => 'Торгове та промислове обладнання',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1594&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/torgovoe-i-promyishlennoe-oborudovanie.html'
                ),
               12 => array(
                    'cat_name' => 'Дім і побут',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1543&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/dom-i-byit.html'
                ),
                13 => array(
                    'cat_name' => 'Спорт, краса',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1572&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/sport-krasota.html'
                ),
                 14 => array(
                    'cat_name' => 'Туризм, подорожі',
                    'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1596&query%5Btag%5D=&query%5Bcity%5D=8',
                    'url' => 'http://catalog.ogo.ua/rv/turizm-puteshestviya.html'
                ),*/
        /*  15 => array(
              'cat_name' => 'Держустанови, комунальні служби',
              'ajax' => 'http://catalog.ogo.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=1582&query%5Btag%5D=&query%5Bcity%5D=8',
              'url' => 'http://catalog.ogo.ua/rv/gosuchrezhdeniya-kommunalnyie-sluzhbyi.html'
          ),
     ),
 ),*/
        /* 5 => array(
             'title' => 'Кропивницький', 'url' => 'https://20.ua/kr', 'categories' => array(
               /*0 => array(
                     'cat_name' => 'Здоров’я',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2309&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/zdorovya/'
                 ),
                 1 => array(
                     'cat_name' => 'Спорт, краса',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2319&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/sport-krasa/'
                 ),
                 2 => array(
                     'cat_name' => 'Дозвілля, розваги',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2381&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/rozvagi/'
                 ),
                 3 => array(
                     'cat_name' => 'Будівництво та ремонт',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2244&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/budivnitstvo-ta-remont/'
                 ),
                 4 => array(
                     'cat_name' => 'Дім і побут',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2335&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/dim-i-pobut/'
                 ),
                 5 => array(
                     'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2341&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/kompyuterna-ofisna-ta-pobutova-tehnika/'
                 ),
                 6 => array(
                     'cat_name' => 'Авто, мото',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2253&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/avto-moto/'
                 ),
               7 => array(
                     'cat_name' => 'Організація заходів',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2263&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/organizatsiya-zahodiv/'
                 ),
                   8 => array(
                     'cat_name' => 'Держустанови, комунальні служби',
                     'ajax' => '',
                     'url' => 'https://20.ua/kr/derzhustanovi-komunalni-sluzhbi/'
                 ),
                9 => array(
                     'cat_name' => 'Навчання, курси',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2359&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/navchannya/'
                 ),
                  10 => array(
                     'cat_name' => 'Бізнес послуги',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2280&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/biznes-poslugi/'
                 ),
                 11 => array(
                     'cat_name' => 'Магазини, супермаркети',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2293&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/magazini-supermarketi/'
                 ),
                 12 => array(
                     'cat_name' => 'Ресторани, кафе, бари',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2366&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/restorani-kafe-bari/'
                 ),
                 13 => array(
                     'cat_name' => 'Туризм, подорожі',
                     'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2372&query%5Btag%5D=&query%5Bcity%5D=10',
                     'url' => 'https://20.ua/kr/turizm-podorozh/'
                 ),
                 14 => array(
                     'cat_name' => 'Торгове та промислове обладнання',
                     'ajax' => '',
                     'url' => 'https://20.ua/kr/torgove-ta-promislove-obladnannya/'
                 ),*/
        /*  15 => array(
              'cat_name' => 'Транспортні послуги',
              'ajax' => '',
              'url' => 'https://20.ua/kr/transportni-poslugi/'
          ),
    ),
),*/
        6 => array(
            'title' => 'Одеса', 'url' => 'https://20.ua/od', 'categories' => array(
                /*  0 => array(
                      'cat_name' => 'Здоров’я',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2151&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/zdorovya/'
                  ),
                  1 => array(
                      'cat_name' => 'Спорт, краса',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2161&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/sport-krasa/'
                  ),
                  2 => array(
                      'cat_name' => 'Дозвілля, розваги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2223&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/rozvagi/'
                  ),
                  3 => array(
                      'cat_name' => 'Будівництво та ремонт',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2086&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/budivnitstvo-ta-remont/'
                  ),
                  4 => array(
                      'cat_name' => 'Дім і побут',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2177&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/dim-i-pobut/'
                  ),
                  5 => array(
                      'cat_name' => 'Комп’ютерна, офісна та побутова техніка',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2183&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/kompyuterna-ofisna-ta-pobutova-tehnika/'
                  ),
                 6 => array(
                      'cat_name' => 'Авто, мото',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2095&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/avto-moto/'
                  ),
                 7 => array(
                      'cat_name' => 'Організація заходів',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2105&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/organizatsiya-zahodiv/'
                  ),
                    8 => array(
                      'cat_name' => 'Держустанови, комунальні служби',
                      'ajax' => '',
                      'url' => 'https://20.ua/od/derzhustanovi-komunalni-sluzhbi/'
                  ),
                 9 => array(
                      'cat_name' => 'Навчання, курси',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2201&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/navchannya/'
                  ),
                   10 => array(
                      'cat_name' => 'Бізнес послуги',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2122&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/biznes-poslugi/'
                  ),
                 11 => array(
                      'cat_name' => 'Магазини, супермаркети',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2135&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/magazini-supermarketi/'
                  ),
                  12 => array(
                      'cat_name' => 'Ресторани, кафе, бари',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2208&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/restorani-kafe-bari/'
                  ),
                   13 => array(
                      'cat_name' => 'Туризм, подорожі',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2214&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/turizm-podorozh/'
                  ),
                  14 => array(
                      'cat_name' => 'Торгове та промислове обладнання',
                      'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2141&query%5Btag%5D=&query%5Bcity%5D=9',
                      'url' => 'https://20.ua/od/torgove-ta-promislove-obladnannya/'
                  ),*/
                15 => array(
                    'cat_name' => 'Транспортні послуги',
                    'ajax' => 'https://20.ua/company-widget/map-list?page=2&order=&query%5Bcategory%5D=2145&query%5Btag%5D=&query%5Bcity%5D=9',
                    'url' => 'https://20.ua/od/transportni-poslugi/'
                ),/**/
            ),
        ),
    ),
);







$items_counter = 2;
$file_name = '';
foreach($citiesInfo['cities'] as $city){
    foreach($city['categories'] as $category){
        $file_name = $city['title'].' '.$category['cat_name'].'.xls';
        echo $category['cat_name']."</br>";
        $html = file_get_html($category['url']);
        if(!empty($html)) {
            $btn = $html->find('a[class="thumbnail pseudo-link load-more js-show-next"]', 0);
            parse_info($html,$objPHPExcel,$category['cat_name'],$items_counter);
            $items_counter +=20;
            sleep(5);
            if ($btn) {
                echo "<br/>" . preg_replace("/[^0-9]/", '', $btn->plaintext);
                $companies_count = preg_replace("/[^0-9]/", '', $btn->plaintext);
                $pages_count = ceil($companies_count / 20) + 1;
                for ($i = 2; $i <= $pages_count; $i++) {
                    $replacement = 'page=' . $i;
                    $page_url = str_replace('page=2', $replacement, $category['ajax']);
                    echo $page_url . "<br/>";
                    $ajax_html = file_get_html($page_url);
                    sleep(5);
                    if(!empty($ajax_html)) {
                        parse_info($ajax_html,$objPHPExcel,$category['cat_name'],$items_counter);
                    }
                    $items_counter +=20;
                }
            }

            echo "Next Category";
        }
    }
}



function parse_info($ajax_html,$objPHPExcel,$category_name,$items_counter){


    $items = $ajax_html->find('div[id="map-list"]', 0);
    $counter = $items_counter;
    foreach ($items->find('div[class="col-sm-6 items list"]') as $details) {
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $counter, $category_name);



        $caption_title = $details->find('a[class="caption-title"]', 0);
        if (!empty($caption_title)) {
            $caption_title = $caption_title->innertext;
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $counter, $caption_title);
            echo $caption_title . "<br/>";
        }

        $attributes = $details->find('a[class="caption-title"]', 0);
        if (!empty($attributes)) {
            $attributes = $attributes->attr;
            echo $attributes['href'] . "</br>";
            //$objPHPExcel->getActiveSheet()->setCellValue('E' . $counter, $attributes['href']);
        }

        $full_address = $details->find('span[class="full full-address"]', 0);
        if (!empty($full_address)) {
            $full_address = $full_address->innertext;
            echo $full_address . "<br/>";
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $counter, $full_address);
        }


        $more_details = $details->find('span[class="normal hidden"] span', 0);
        if (!empty($more_details)) {
            $more_details = $more_details->innertext;
            echo "<br/>" . $more_details;
            $objPHPExcel->getActiveSheet()->setCellValue('E' . $counter, $more_details);
        }


        if (!empty($attributes['href'])) {
            $base = $attributes["href"];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_URL, $base);
            curl_setopt($curl, CURLOPT_REFERER, $base);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            $str = curl_exec($curl);
            curl_close($curl);

            $item_details_page = new simple_html_dom();
            $item_details_page->load($str);

            /*$description = $item_details_page->find('div[id="company_full_description"]', 0);
            if (!empty($description)) {
                $description = $description->innertext;
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $counter, $description);
            }*/

            $site_link = $item_details_page->find('a[class="ga-company-site pseudo-link"]', 0);
            if (!empty($site_link)) {
                $the_link = $site_link->attr;
                $the_link = $the_link['href'];
                echo "<br/>".$the_link;
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $counter, $the_link);
            }

            $social_links_block = $item_details_page->find('div[id="company-contacts-social-collapse"]', 0);
            if (!empty($social_links_block)) {
                $links_list = '';
                foreach ($social_links_block->find('div[class="panel-body"]') as $link) {
                    $link = $link->find('a[class="panel-title"]', 0)->attr;
                    echo "<br/>" . $link['href'];
                    $links_list .= $link['href']."\n";
                }
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $counter, $links_list);
            }


            $company_email = $item_details_page->find('span[class="full hidden"]', 0);
            if (!empty($company_email)) {
                echo "</br>***** $company_email->innertext *****</br>";
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $counter, $company_email->innertext);
            }

        }

        echo "</br>*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|*|<br/><br/>";
        $objPHPExcel->getActiveSheet()->getRowDimension($counter)->setRowHeight(35);
        $counter++;

    }

}





$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

// Rename worksheet
echo date('H:i:s') , " Rename worksheet" , EOL;
$objPHPExcel->getActiveSheet()->setTitle('Компанії');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save(str_replace('.php', '.xls', __FILE__));
$objWriter->save($file_name);
echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


echo date('H:i:s') , " Reload workbook from saved file" , EOL;
$callStartTime = microtime(true);


echo 'File has been created in ' , getcwd() , EOL;


function debug($var){
    echo "<pre>";print_r($var);echo "</pre>";
}