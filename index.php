<?php
include_once('simple_html_dom.php');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('max_execution_time', 9000);
date_default_timezone_set('Europe/Kiev');

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("OT")
    ->setLastModifiedBy("PBlazhuk")
    ->setTitle("Companies list")
    ->setSubject("Companies")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

// Set default font
echo date('H:i:s') , " Set default font" , EOL;
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
    ->setSize(10);

// Add some data, resembling some different data types
echo date('H:i:s') , " Add some data" , EOL;

$key_cell = 'A4';
$counter = 5;



$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Ім`я')
    ->setCellValue('B1', 'Місто')
    ->setCellValue('C1', 'Номер телефону')
    ->setCellValue('D1', 'Марка авто')
    ->setCellValue('E1', 'Модель авто')
    ->setCellValue('F1', 'Рік випуску')
    ->setCellValue('G1', 'Ціна');












$html = file_get_html('https://auto.ria.com/search/?categories.main.id=1&region.id[0]=10&city.id[0]=10&price.USD.gte=8000&price.USD.lte=20000&price.currency=1&top=1&abroad.not=0&custom.not=1&page=0&size=100');



$items = $html->find('div[id="searchResults"]', 0);


$counter = 2;
foreach ($items->find('div[class="content-bar"]') as $car_item){

    echo $car_item.'-----<br/>';
    $car_title = $car_item->find('div[class="item ticket-title"]',0);
    $car_title = $car_title->find('a[class="address"]',0)->attr;
    echo $car_title['title'];

    $parsed_title = explode(' ',$car_title['title']);

    $marka = $parsed_title[0];
    $objPHPExcel->getActiveSheet()->setCellValue('D' . $counter, $marka);
    $year = $parsed_title[count($parsed_title) - 1];
    $objPHPExcel->getActiveSheet()->setCellValue('F' . $counter, $year);

    unset($parsed_title[count($parsed_title) - 1]);
    unset($parsed_title[0]);


    $model = implode(' ',$parsed_title);
    $objPHPExcel->getActiveSheet()->setCellValue('E' . $counter, $model);



    $detail_page_url = $car_item->find('a[class="m-link-ticket"]',0)->attr;
    $detail_page_url = $detail_page_url['href'];
    echo $detail_page_url;


    $car_price = $car_item->find('div[class="price-ticket"]',0);
    $car_price = $car_price->find('span[class="size15"]',0);
    $car_price = $car_price->find('span[class="bold green size22"]',0)->innertext;
    echo "<br/>".$car_price;

    $objPHPExcel->getActiveSheet()->setCellValue('G' . $counter, $car_price." $");


    echo "<br/>******************************<br/>";

    $details = file_get_html($detail_page_url);


    $user_info  = $details->find('div[class="user-contact"]',0);
   // echo $user_info;

    $phone = $user_info->find('div[class="item-phone"]',0);
    $phone = $user_info->find('div[class="phone"]',0);
    $phone = $phone->find('span',0)->attr;
    echo "<br/>*****************<br/>".$phone['data-phone-number'];

    $objPHPExcel->getActiveSheet()->setCellValue('C' . $counter, $phone['data-phone-number']);


    echo "<br/>";

    $user_name = $user_info->find('dt[class="user-name"]',0);
    echo $user_name;

    $person_info = explode(',',strip_tags($user_name));



    if(count($person_info)>1) {
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $counter, $person_info[0]);
        if (!empty($person_info[1])) {
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $counter, trim($person_info[1]));
        }
    }else{
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $counter, trim($person_info[0]));
    }



    echo '<br/>///////////<br/>';

    $objPHPExcel->getActiveSheet()->getRowDimension($counter)->setRowHeight(25);
    $counter++;
}







$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

// Rename worksheet

echo date('H:i:s') , " Rename worksheet" , EOL;
$objPHPExcel->getActiveSheet()->setTitle('Автомобілі');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Save Excel 95 file
echo date('H:i:s') , " Write to Excel5 format" , EOL;
$callStartTime = microtime(true);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//$objWriter->save(str_replace('.php', '.xls', __FILE__));
$file_name = 'AUTO RIA '.date('H:i:s').'.xls';
$objWriter->save($file_name);
echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;

echo date('H:i:s') , " File written to " , str_replace('.php', '.xls', pathinfo(__FILE__, PATHINFO_BASENAME)) , EOL;
echo 'Call time to write Workbook was ' , sprintf('%.4f',$callTime) , " seconds" , EOL;
// Echo memory usage
echo date('H:i:s') , ' Current memory usage: ' , (memory_get_usage(true) / 1024 / 1024) , " MB" , EOL;


echo date('H:i:s') , " Reload workbook from saved file" , EOL;
$callStartTime = microtime(true);


echo 'File has been created in ' , getcwd() , EOL;


function debug($var){
    echo "<pre>";print_r($var);echo "</pre>";
}